package pl.com.damdor.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.InvocationTargetException;

/**
 * Base adapter class to use with custom elementy type.
 */

public abstract class CustomViewItemAdapter<Model, ViewType> extends ItemAdapter<Model> {

    private Class<ViewType> viewClass;

    public CustomViewItemAdapter(Context context, Class<Model> modelClass, Class<ViewType> viewClass){
        super(context, modelClass);
        this.viewClass = viewClass;
    }

    @Override
    public View getView(Model model, int position, View convertView, ViewGroup parent) {
        ViewType view = null;

        if(convertView != null){
            view = (ViewType) convertView;
        } else {
            view = createView();
        }

        setupModel(view, model, position);
        return (View) view;
    }

    /**
     * Creates new element. Default implementation uses one parameter constructor, passing @see {@link Context}.
     * If your class does not have that constructor or you want to use other, you should override this method.
     * @return
     */
    protected ViewType createView() {
        try {
            return (ViewType) viewClass.getDeclaredConstructor(Context.class).newInstance(getContext());
        } catch (Exception e) {
            throw new RuntimeException("Cannot instantiate class " + viewClass.getName(), e);
        }
    }
    protected abstract void setupModel(ViewType view, Model model, int position);

}
