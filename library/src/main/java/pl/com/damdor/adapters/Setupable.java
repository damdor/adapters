package pl.com.damdor.adapters;

/**
 * Created by Damian Doroba on 2017-04-04.
 */

public interface Setupable<Model> {
    void setup(Model model);
}
