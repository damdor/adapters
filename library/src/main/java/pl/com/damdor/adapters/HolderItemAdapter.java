package pl.com.damdor.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Adapter class for ViewHolder pattern. For more information see this
 * @see https://developer.android.com/training/improving-layouts/smooth-scrolling.html
 */

public abstract class HolderItemAdapter<Model, HolderModel> extends ItemAdapter<Model> {

    private @LayoutRes int layoutId;

    public HolderItemAdapter(Context context, Class<Model> modelClass, @LayoutRes int layoutId){
        super(context, modelClass);
        this.layoutId = layoutId;
    }

    @Override
    public View getView(Model model, int position, View convertView, ViewGroup parent){
        View view;
        HolderModel holder;

        if(convertView != null){
            view = convertView;
            holder = (HolderModel) view.getTag();
        } else {
            view = LayoutInflater.from(getContext()).inflate(layoutId, parent, false);
            holder = createHolder();
            populateHolder(view, holder, position);
            view.setTag(holder);
        }
        setupModel(view, holder, model, position);
        return view;
    }


    /**
     * Use this method to setup holder fields with views
     * @param view
     * @param holder
     * @param position
     */
    protected abstract void populateHolder(View view, HolderModel holder, int position);

    /**
     * Use this method to setup views with model data
     * @param view
     * @param holder
     * @param position
     */
    protected abstract void setupModel(View view, HolderModel holder, Model model, int position);

    /**
     * Create empty holder instance
     * @return
     */
    protected abstract HolderModel createHolder();

}
