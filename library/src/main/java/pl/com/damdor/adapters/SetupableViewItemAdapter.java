package pl.com.damdor.adapters;

import android.content.Context;

/**

 */

public class SetupableViewItemAdapter<Model, ViewType extends Setupable<Model>> extends CustomViewItemAdapter<Model, ViewType> {

    public SetupableViewItemAdapter(Context context, Class<Model> modelClass, Class<ViewType> viewClass) {
        super(context, modelClass, viewClass);
    }

    @Override
    protected void setupModel(ViewType view, Model model, int position) {
        view.setup(model);
    }
}
