package pl.com.damdor.adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

/**
 * Base adapter class
 */

public abstract class ItemAdapter<Model> {

    private Context context;
    private Class<Model> modelClass;

    /**
     * Creates adapter
     * @param context Context of application
     * @param modelClass Class object
     */
    public ItemAdapter(Context context, Class<Model> modelClass){
        this.context = context;
        this.modelClass = modelClass;
    }

    /**
     * Creates view for this adapter
     * @param model
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    public abstract View getView(Model model, int position, View convertView, ViewGroup parent);

    /**
     * Check if model can be accepted by this adapter. This method should not be overriden
     * @param object
     * @retur
     */
    public boolean accept(Object object) {
        if(!modelClass.isAssignableFrom(object.getClass())){
            return false;
        }
        Model model = (Model) object;
        return acceptModel(model);
    }

    /**
     * * Check if model can be accepted by this adapter. You should override this method for filter
     * models
     * @param model
     * @return
     */
    protected boolean acceptModel(Model model){
        return true;
    }

    public  Context getContext(){
        return context;
    }

}
