package pl.com.damdor.adapters;

import android.view.View;
import android.view.ViewGroup;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Damian Doroba on 2017-03-23.
 */

public class ItemAdapterRouter {

    private Iterable<ItemAdapter> adapters;
    private Map<ItemAdapter, Integer> adapterToItemId;

    public ItemAdapterRouter(Iterable<ItemAdapter> adapters){
        this.adapters = adapters;
        adapterToItemId = new HashMap<ItemAdapter, Integer>();

        int nextId = 0;
        for(ItemAdapter adapter : adapters){
            adapterToItemId.put(adapter, nextId++);
        }
    }

    public ItemAdapterRouter(ItemAdapter[] adapters){
        this(Arrays.asList(adapters));
    }

    public View getView(Object model, int position, View convertView, ViewGroup parent) {
        ItemAdapter adapter = findAdapter(model);
        return adapter.getView(model, position, convertView, parent);
    }

    public int getItemId(Object model){
        return adapterToItemId.get(findAdapter(model));
    }

    private ItemAdapter findAdapter(Object model){
        for(ItemAdapter adapter : adapters){
            if(adapter.accept(model)){
                return adapter;
            }
        }

        throw new RuntimeException("No item adapter found");
    }

}
